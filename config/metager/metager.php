<?php

return [
    "browserverification_enabled" => true,
    "browserverification_whitelist" => [
        "w3m\/",
    ],
];
