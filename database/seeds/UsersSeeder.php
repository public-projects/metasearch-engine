<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();
        DB::table('users')->insert(
            [
                'email' => 'admin',
                'name' => 'admin',
                'password' => Hash::make('admin'),
            ]
        );
    }
}
