<!DOCTYPE html>
<html lang="{!! trans('staticPages.meta.language') !!}">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="/index.css?id={{ $key }}" beforeLoad="console.log('test');">
    <script src="/index.js?id={{ $key }}"></script>
