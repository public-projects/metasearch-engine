<?php return [
    "foki.web" => "Web",
    "foki.bilder" => "Imágenes",
    "foki.nachrichten" => "Informativos/Política",
    "foki.wissenschaft" => "Ciencia",
    "foki.produkte" => "Productos",
    "foki.angepasst" => "ajustado",
    "foki.maps" => "Maps.metager.de",
    "design" => "Escoger un diseño personalizado",
    "conveyor" => "Comprar en tiendas que apoyan MetaGer ",
    "partnertitle" => "Apoyar MetaGer, sin costos para usted",
    "mapstitle" => "MetaGer - Servicio de tarjeta",
    "plugin" => "Añadir MetaGer-Plugin",
    "plugin-title" => "Añadir MetaGer a su navegador",
    "options.head" => "Administrar filtros",
    "focus-creator.head" => "Adaptar búsqueda",
    "focus-creator.description" => "Compilar motores de búsqueda",
    "focus-creator.name-placeholder" => "Nombre de mis propios de búsqueda",
    "focus-creator.save" => "Hacer búsqueda con mis propios de búsqueda",
    "focus-creator.delete" => "Borrar mis propios de búsqueda",
    "about.1.1" => "Privacidad garantizada",
    "about.1.2" => "Con nosotros tiene control total sobre sus datos. No rastreamos y nuestro código fuente es gratuito.",
    "about.2.1" => "Diversa y libre",
    "about.2.2" => "MetaGer protege contra la censura al combinar los resultados de múltiples motores de búsqueda.",
    "about.3.1" => "Energía 100% renovable",
    "about.3.2" => "Todos nuestros servicios se ejecutan con energía renovable. Sostenible y limpio.",
    "about.4.1" => "Organización sin ánimo de lucro",
    "about.4.2" => "¡Fortalécenos convirtiéndote en miembro o donando a nuestra organización sin fines de lucro!",
    "lang.tooltip" => "Idioma de resultados",
    "key.placeholder" => "Llave para buscar sin publicidad",
    "key.tooltip" => "Llave para buscar sin publicidad",
    "placeholder" => "MetaGer: Buscar & encontrar seguro",
    "searchbutton" => "MetaGer - Búsqueda",
    "tooltips.add-focus" => "Adaptar búsqueda",
    "tooltips.edit-focus" => "Labra el atención actual",
    "tooltips.settings" => "Ajuste general",
];
