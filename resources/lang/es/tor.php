<?php

return [
    'description' => 'A continuación, encontrará el enlace al servicio oculto de MetaGer. Atención: Solo se puede acceder a este enlace a través de la red Tor. Su navegador genera un mensaje de error si no está conectado a él.',
    "torbutton" => "Usar Tor",
];
