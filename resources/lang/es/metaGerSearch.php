<?php return [
    "quicktips.wikipedia.adress"	=>	"de <a href=\"https://es.wikipedia.org\" target=\"_blank\" rel=\"noopener\">Wikipedia, la enciclopedia libre</a>",
    "quicktips.dictcc.adress"	=>	"de <a href=\"https://www.dict.cc/\" target=\"_blank\" rel=\"noopener\">dict.cc</a>",
    "quicktips.tips.title"	=>	"¿Sabía usted?",
    "quicktips.bang.title"	=>	"Reenvío !bang",
    "quicktips.bang.buttonlabel"	=>	"seguir haciendo buscar:",
    "quicktips.bang.from"	=>	"de"
];
