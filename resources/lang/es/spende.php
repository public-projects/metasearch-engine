<?php return [
    "headline.1"	=>	"Su donación para SUME-EV y MetaGer",
    "headline.2"	=>	"Ayuda usted, que en el internet los buscadores libres quedan libre. El conocimiento digital del mundo tiene que ser accesible sin tutela de estados o empresas.",
    "bankinfo.1"	=>	"Con una transferencia bancaria",
    "bankinfo.2"	=>	"SUMA-EV",
    "bankinfo.2.1"	=>	"IBAN: DE64 4306 0967 4075 0332 01",
    "bankinfo.2.2"	=>	"BIC: GENODEM1GLS",
    "bankinfo.2.3"	=>	"Banco: GLS Gemeinschaftsbank, Bochum",
    "bankinfo.2.4"	=>	"(NDC: 4075 0332 01, Código: 43060967)",
    "bankinfo.3"	=>	"En caso que quiere un recibo de donación, por favor ponga su correo electrónico y su dirección completa en el formulario de transferencia.",
    "lastschrift.1"	=>	"Donaciones con procedimiento de nota de cargo:",
    "lastschrift.2"	=>	"Simplemente llena el formulario con sus datos bancarios y el monto deseado. Nosotros entonces cargamos su cuenta acuerdo a los datos proporcionados.",
    "lastschrift.3"	=>	"Por favor ponga su nombre:",
    "lastschrift.3.placeholder"	=>	"Nombre",
    "lastschrift.4"	=>	"Su correo electrónico:",
    "lastschrift.5"	=>	"Su numero de teléfono, para verificar su donación vía una llamada si fuese necesario:",
    "lastschrift.6"	=>	"Su IBAN o numero de cuenta:",
    "lastschrift.7"	=>	"Su BIC o \tcódigo de identificación bancaria:",
    "lastschrift.8.value"	=>	"Aquí puede entrar su donación:",
    "lastschrift.8.value.placeholder"	=>	"Monto de donación",
    "lastschrift.8.message"	=>	"Aquí puede enviarnos un mensaje adicional si quiere:",
    "lastschrift.8.message.placeholder"	=>	"Mensaje",
    "lastschrift.9"	=>	"Donación",
    "lastschrift.10"	=>	"Sus datos serán transmitidos por una conexión encryptada y no pueden ser leído por terceros. El monto que usted nos indica, sera cargado a su cuenta. SUME-EV usa sus datos exclusivamente para la contabilidad de las donaciones. Sus datos no serán transmitidos a nadie. Donaciones al SUMA-EV son deducibles de los impuestos, ya que el SUME-EV es aprobado por el Finanzamt Hannover Nord como asociación sin fines de lucro, y esta registrado en el registro del Amtsgericht Hannover con el numero VR200033. Un recibo de donaciones arriba de 200,- Euro será mandado automáticamente. Para donaciones hasta 200,- EURO basta el extracto de cuenta para deducirlo de los impuestos.",
    "drucken"	=>	"Imprimir",
    "danke.title"	=>	"¡Muchas gracias!! Hemos recibido su informacion de donación para MetaGer al SUME-EV.",
    "danke.nachricht"	=>	"En caso que nos ha enviado datos de contacto, pronto va recibir un mensaje personalizado.",
    "danke.kontrolle"	=>	"Hemos recibido el siguiente mensaje:",
    "telefonnummer"	=>	"Teléfono",
    "iban"	=>	"IBAN/ No. de cuenta",
    "bic"	=>	"BIC o código de identificación bancaria:",
    "betrag"	=>	"Cantidad",
    "danke.message"	=>	"Su mensaje",
    "paypal.title"	=>	"Donar con Paypal",
    "paypal.subtitle"	=>	"Allí con tarjeta de crédito sin registro",
    "paypal.2"	=>	"Donar con Paypal - rapido, seguro y gratis!",
    "bitpay.title"	=>	"Donar con Bitpay"
];
