<?php return [
    "head.1"	=>	"Asociador - MetaGer",
    "1.1"	=>	"No sabe otras palabras para buscar? Pregunta nuestro asociador.",
    "1.2"	=>	"Protección de Datos",
    "reasso.title"	=>	"Asociar esto término",
    "searchasso.title"	=>	"Buscar (MetaGer) con esto término",
    "search.placeholder"	=>	"Término para asociar"
];
