<?php

return [
    "headline.1" => "Contacto",

    "form.1"     => "Formulario de contacto seguro",
    "form.2"     => "Con este formulario nos puede contactar encryptado. Por supuesto como vía alterna nos puede  mandar un <a href=\"mailto:office@suma-ev.de\">email</a>.",
    "form.5"     => "Su dirección de correo electrónico (opcional)",
    "form.6"     => "Su mensaje",
    'form.7'     => '<strong>Su mensaje será encryptada antes de mandarla <a href="http://openpgpjs.org/.">OpenPGP.js</a> para esto necesitamos Javascript.</strong> Sino tiene activado Javascript su mensaje será enviada sin encryptación.',
    "form.8"     => "Encriptar y enviar",

    "mail.1"     => "Por email",
    "mail.2"     => 'Nos puede mandar un email directamente a la siguiente direccion: <a href="mailto::mail">:mail</a>',
    "mail.3"     => "Si la quiere encriptar, puede ver nuestra llave pública de OpenPGP aquí:",

    "letter.1"   => "Por carta",
    "letter.2"   => "Preferimos que nos contacte por medios digitales. Si lo ve indispensable contactarnos vía correo fisico, nos puede escribir a la siguiente dirección:",
    "letter.3"   => "SUMA-EV Röselerstr. 3 30159 Hannover Germany",
    'error.1'    => "Lo sentimos, pero desafortunadamente no recibimos ningún dato con su solicitud de contacto. El mensaje no fue enviado.",
    'success.1'  => "Su mensaje nos fue enviado con éxito. ¡Muchas gracias por esto! Procesaremos esto lo antes posible y luego lo contactaremos nuevamente si es necesario."
];
