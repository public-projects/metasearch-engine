<?php
return [
    '1' => 'Perdon por la molestia',
    '2' => 'Estás en una red desde la que recibimos cada vez más consultas automáticas. No se preocupe: esto no significa necesariamente que estas solicitudes provengan de su PC.',
    '3' => 'Sin embargo, no podemos distinguir sus peticiones de las del "robot". Para proteger los motores de búsqueda que consultamos, debemos asegurarnos de que no estén inundados de consultas (automatizadas).',
    '4' => 'Ingrese los caracteres de la imagen en el cuadro de entrada y confirme con "OK" para llegar a la página de resultados.',
    '5' => 'Entrar en captcha',
    '6' => 'Entrar en día laborable',
];
