<?php

return [
    'h1' => "Schlüssel für Ihre werbefreie Suche",
    'p1' => 'MetaGer bietet <a href=":url1">SUMA-EV Mitgliedern</a> und großzügigen <a href=":url2">Spendern</a> einen Schlüssel an, mit dem sie Zugriff auf ein Kontingent an werbefreien Suchen haben.',
    'p2' => 'Auf dieser Seite können Sie Ihren Schlüssel (sofern bekannt) eingeben. Wir speichern diesen mit Hilfe eines Cookies auf Ihrem PC. Auf diese Weise sendet Ihr Browser den Schlüssel automatisch bei jeder durchgeführten Suche an uns, sodass wir die Werbung für Sie entfernen können.',
    'p3' => 'Wenn Sie sich den Cookie anschauen steht dort drin "key=xxxx". Wir verwenden diesen dementsprechend nicht für Tracking-Zwecke. Er wird auch zu keinem Zeitpunkt in irgendeiner Form von uns gespeichert oder geloggt.',
    'p4' => 'Wichtig: Um diese Funktion nutzen zu können, müssen Sie Cookies in Ihrem Browser zugelassen haben. Die Einstellung bleibt dann solange gespeichert, wie Ihr Browser Cookies speichert.',
    'p5' => 'Um den Schlüssel darüber hinausgehend speichern zu können haben Sie folgende Möglichkeiten:',
    'li1' => 'Richten Sie sich <code>:url</code>',
    'li2' => 'Falls Sie MetaGer als Standardsuchmaschine verwenden, können Sie nach der Schlüsseleingabe den Eintrag löschen und erneut hinzufügen. Wenn Sie in Ihrem Browser einen "Suchstring" eingeben müssen, können Sie <code>:url</code> verwenden.',
    'placeholder1' => 'Schlüssel eingeben...',
    'removeKey' => 'aktuellen Schlüssel entfernen',
    'invalidKey' => 'Der eingegebene Schlüssel ist ungültig',
    'backLink' => 'Zurück zur letzten Seite',
];
