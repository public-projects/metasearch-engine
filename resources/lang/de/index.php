<?php

return [
    'foki.web' => 'Web',
    'foki.bilder' => 'Bilder',
    'foki.nachrichten' => 'News/Politik',
    'foki.wissenschaft' => 'Wissenschaft',
    'foki.produkte' => 'Produkte',
    'foki.angepasst' => 'Angepasst',
    'foki.maps' => 'Maps.metager.de',

    'design' => 'Persönliches Design auswählen',

    'conveyor' => 'Einkaufen bei MetaGer-Fördershops',
    'partnertitle' => 'MetaGer unterstützen, ohne Mehrkosten für Sie',
    'mapstitle' => 'Der MetaGer Kartenservice',

    'plugin' => 'MetaGer-Plugin hinzufügen',
    'plugin-title' => 'MetaGer zu Ihrem Browser hinzufügen',

    'options.head' => 'Filter verwalten',

    'focus-creator.head' => 'Suche anpassen',
    'focus-creator.description' => 'Suchen Sie sich aus unseren Suchmaschinen Ihre eigene Suche zusammen.',
    'focus-creator.name-placeholder' => 'Name der eigenen Suche',
    'focus-creator.save' => 'Eigene Suche durchführen',
    'focus-creator.delete' => 'Eigene Suche löschen',
    'focus-creator.focusname' => 'Suchname: ',

    'about.1.1' => "Garantierte Privatsphäre",
    'about.1.2' => 'Mit uns behalten Sie die volle Kontrolle über Ihre Daten. Wir speichern nicht und der Quellcode ist frei.',
    'about.2.1' => 'Vielfältig & Frei',
    'about.2.2' => 'MetaGer schützt gegen Zensur, indem es Ergebnisse vieler Suchmaschinen kombiniert.',
    'about.3.1' => '100% Ökostrom',
    'about.3.2' => 'Alle unsere Dienste sind mit Strom aus regenerativen Energiequellen betrieben. Nachhaltig und sauber.',
    'about.4.1' => 'Gemeinnütziger Verein',
    'about.4.2' => 'Unterstützen Sie MetaGer, indem Sie spenden oder Mitglied im gemeinnützigen Trägerverein werden.',

    'lang.tooltip' => 'Ergebnissprache wählen',
    'key.placeholder' => 'Schlüssel für werbefreie Suche eingeben',
    'key.tooltip' => 'Schlüssel für werbefreie Suche eingeben',
    'placeholder' => 'MetaGer: Sicher suchen & finden, Privatsphäre schützen',
    'searchbutton' => 'MetaGer-Suche',

    'tooltips.add-focus' => 'Suche anpassen',
    'tooltips.edit-focus' => 'Aktuellen Fokus bearbeiten',
    'tooltips.settings' => 'Allgemeine Einstellungen',
];
