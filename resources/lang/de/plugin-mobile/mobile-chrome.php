<?php
    return[

        'default-search-v83.1' => 'Tippen Sie in Ihrem Browser oben rechts auf "<i class="fas fa-ellipsis-v"></i>" um das Browsermenü zu öffnen.',
        'default-search-v83.2' => 'Wählen Sie den Menüpunkt "Einstellungen".',
        'default-search-v83.3' => 'In dem dann erscheinenden Menü wählen Sie in der Kategorie "Grundeinstellungen" den Menüpunkt "Suchmaschine".',
        'default-search-v83.4' => 'Tippen Sie in der Kategorie "Kürzlich besucht" auf MetaGer.',
        'default-search-v83.5' => 'Sollte der MetaGer-Eintrag bei Ihnen nicht vorhanden sein, nutzen Sie die Suchleiste unterhalb der Anleitung für eine Suchanfrage und versuchen Sie es erneut.',

        'default-page-v83.1' => 'Tippen Sie in Ihrem Browser oben rechts auf "<i class="fas fa-ellipsis-v"></i>" um das Browsermenü zu öffnen.',
        'default-page-v83.2' => 'Wählen Sie den Menüpunkt "Einstellungen".',
        'default-page-v83.3' => 'In dem dann erscheinenden Menü wählen Sie in der Kategorie "Erweitert" den Menüpunkt "Startseite".',
        'default-page-v83.4' => 'Tippen Sie auf "Benutzerdefinierte Webadresse eingeben" und tragen ":link" ein.',

    ];