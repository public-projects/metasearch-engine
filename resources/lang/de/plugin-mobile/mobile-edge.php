<?php
    return[

        'default-search-v45.1' => 'Tippen Sie in Ihrem Browser unten in der Mitte auf "<i class="fas fa-ellipsis-h"></i>" um das Browsermenü zu öffnen.',
        'default-search-v45.2' => 'Wählen Sie den Menüpunkt Einstellungen auf der rechten Seite des erscheinenden Menüs.',
        'default-search-v45.3' => 'Scrollen Sie nach unten und tippen in der Kategorie "Erweitert" auf "Suchen".',
        'default-search-v45.4' => 'Tippen Sie auf "Standardsuchmaschine" und wählen Sie in der Kategorie "Kürzlich besucht" MetaGer aus.',
        'default-search-v45.5' => 'Sollte der MetaGer-Eintrag bei Ihnen nicht vorhanden sein, nutzen Sie die Suchleiste unterhalb der Anleitung für eine Suchanfrage und versuchen Sie es erneut.',

        'default-page-v45.1' => 'Klicken Sie <a href="/" target="_blank" rel="noopener">hier</a> um MetaGer in einem neuen Tab zu öffnen.',
        'default-page-v45.2' => 'Tippen Sie in Ihrem Browser unten in der Mitte auf "<i class="fas fa-ellipsis-h"></i>" um das Browsermenü zu öffnen.',
        'default-page-v45.3' => 'Tippen Sie in der Kategorie "Grundlegend" auf "Startseite".',
        'default-page-v45.4' => 'Wählen Sie "Bestimmte Seite" und tippen Sie auf "Aktuelle Seite verwenden" und anschließend auf "Speichern".',
    ];