<?php
    return[

        'search-string' => 'Such-String:',

        //Instructions for Firefox Mobile version < 80
        'default-search-vlt80.1' => 'Tippen und halten Sie mit dem Finger auf dem unten stehenden Suchfeld gedrückt.',
        'default-search-vlt80.2' => 'Im geöffneten Kontextmenü können Sie "Suchmaschine hinzufügen" auswählen',
        'default-search-vlt80.3' => 'In den Browsereinstellungen haben Sie nun die Möglichkeit im Bereich "Suchen" MetaGer als Standardsuchmaschine zu wählen.',

        //Instructions for Firefox Mobile version >= 80
        'default-search-v80.1' => 'Tippen Sie in Ihrem Browser unten rechts auf "<i class="fas fa-ellipsis-v"></i>" um das Browsermenü zu öffnen.',
        'default-search-v80.2' => 'Wählen Sie den Menüpunkt Einstellungen.',
        'default-search-v80.3' => 'In dem dann erscheinenden Menü wählen Sie in der Kategorie "Allgemein" den Menüpunkt "Suchen".',
        'default-search-v80.4' => 'Tippen Sie auf "+ Suchmaschine hinzufügen".',
        'default-search-v80.5' => 'Wählen Sie "Andere" aus und tragen darunter den Namen (MetaGer) und den Such-String ein. Bestätigen Sie mit "<i class="fas fa-check"></i>" in der rechten oberen Ecke.',
    ];