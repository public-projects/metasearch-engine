<?php
    return[

        'default-search-v60.1' => 'Tippen und halten Sie mit dem Finger auf das Sucheingabefeld unter der Anleitung.',
        'default-search-v60.2' => 'Wählen Sie im erscheinenden Menü "Suchmaschine hinzufügen...".',
        'default-search-v60.3' => 'Tippen Sie im Popup auf "OK".',
        'default-search-v60.4' => 'Nun können Sie in einem neuen Tab durch das Tippen auf das Logo Ihrer Standardsuchmaschine MetaGer auswählen',        
        //link to mobile-unable
    ];