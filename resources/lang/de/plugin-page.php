<?php

return [
    'open-modal' => 'MetaGer-Plugin hinzufügen',
    'open-modal.title' => 'MetaGer zu Ihrem Browser hinzufügen',
    'head.0' => 'Firefox installieren und MetaGer hinzufügen',
    'head.1' => 'MetaGer zum Firefox hinzufügen',
    'head.2' => 'MetaGer zum Chrome hinzufügen',
    'head.3' => 'MetaGer zum Opera hinzufügen',
    'head.4' => 'MetaGer zum Internet Explorer hinzufügen',
    'head.5' => 'MetaGer zum Microsoft Edge hinzufügen',
    'head.6' => 'MetaGer zum Safari hinzufügen',
    'head.7' => 'MetaGer zum Vivaldi hinzufügen',
    'head.8' => 'MetaGer zum Firefox Klar hinzufügen',
    'head.9' => 'MetaGer zum UCBrowser hinzufügen',
    'head.10' => 'Metager zum Samsung hinzufügen',

    'browser-download' => 'Firefox herunterladen',
    'firefox-plugin' => 'MetaGer als Standardsuchmaschine mit Add-on einrichten',
    'firefox-default-search' => 'MetaGer als Standardsuchmaschine ohne Add-on einrichten',
    'default-search' => 'MetaGer als Standardsuchmaschine einrichten',
    'default-page' => 'MetaGer als Startseite einrichten',

    //Unable: Opera Desktop, Safari Desktop, UC Browser Mobile, Safari Mobile
    'desktop-unable' => 'Leider bietet der von Ihnen genutzte Browser nicht die Möglichkeit MetaGer als (Standard-)Suchmaschine einzustellen, aber <a href="https://www.mozilla.org/de/firefox/new/" target="_blank" rel="noopener">hier</a> können Sie mit Firefox einen Open-Source Browser installieren, für den wir eine Anleitung haben.',
    'mobile-unable' => 'Leider bietet der von Ihnen genutzte Browser nicht die Möglichkeit MetaGer als (Standard-)Suchmaschine einzustellen, aber <a href="https://www.mozilla.org/de/firefox/mobile/" target="_blank" rel="noopener">hier</a> können Sie mit Firefox einen Open-Source Browser installieren, für den wir eine Anleitung haben.',
    
    'desktop-notlisted' => 'Leider ist uns bisher nicht bekannt, wie die (Standard-)Suchmaschine in dem von Ihnen genutzen Browser einzustellen ist, aber <a href="https://www.mozilla.org/de/firefox/mobile/" target="_blank" rel="noopener">hier</a> können Sie mit Firefox einen Open-Source Browser installieren, bei dem wir es wissen.' ,
    'mobile-notlisted' => 'Leider ist uns bisher nicht bekannt, wie die (Standard-)Suchmaschine in dem von Ihnen genutzen Browser einzustellen ist, aber <a href="https://www.mozilla.org/de/firefox/mobile/" target="_blank" rel="noopener">hier</a> können Sie mit Firefox einen Open-Source Browser installieren, bei dem wir es wissen.' ,
];
