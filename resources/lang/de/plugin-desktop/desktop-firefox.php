<?php
    return[

        //Instructions for Firefox version 52-56
        'default-search-v52.1' => 'Klicken Sie oben rechts neben der Adressleiste auf die Lupe mit dem Plussymbol.',
        'default-search-v52.2' => 'Wählen Sie im erscheinenden Menü das MetaGer-Logo mit dem Plussymbol.',
        'default-search-v52.3' => 'Geben Sie "about:preferences#search" in die Adressleiste ein um in die Sucheinstellungen zu gelangen.',
        'default-search-v52.4' => 'Im Dropdownmenü unter "Standardsuchmaschine" steht Ihnen nun MetaGer zur Verfügung.',

        //Instructions for Firefox version 57-60
        'default-search-v57.1' => 'Geben Sie "about:preferences#search" in der Adressleiste ein um in die Sucheinstellungen zu gelangen.',
        'default-search-v57.2' => 'Wählen Sie in den erscheinenden Einstellungen "Suchleiste zur Symbolleiste hinzufügen".',
        'default-search-v57.3' => 'Wechseln Sie zurück in den Tab, in dem MetaGer offen ist und klicken Sie oben rechts neben der Adressleiste auf die Lupe mit dem Plussymbol.',
        'default-search-v57.4' => 'Klicken Sie im erscheinenden Menü auf das MetaGer-Logo mit dem Plussymbol.',
        'default-search-v57.5' => 'Gehen Sie wieder in den Tab mit den geöffneten Einstellungen. Im Dropdownmenü unter "Standardsuchmaschine" steht Ihnen nun MetaGer zur Verfügung.',

        //Instructions for Firefox version >= 61
        'plugin' => 'Das Plugin für Firefox können Sie <a href="https://addons.mozilla.org/de/firefox/addon/metager-suche/?src=search" target="_blank">hier</a> herunterladen.',
        'default-search-v61.1' => 'Alternativ klicken Sie in Ihrem Browser oben rechts in der Adressleiste auf "<i class="fas fa-ellipsis-h"></i>" um das Menü der Seitenaktionen zu öffnen und dann auf "Suchmaschine hinzufügen"',
        'default-search-v61.2' => 'Geben Sie "about:preferences#search" in der Adressleiste ein um in die Sucheinstellungen zu gelangen. Im Bereich "Standardsuchmaschine" wählen Sie MetaGer aus.',

        //Instructions for Firefox version 52-60
        'default-page-v52.1' => 'Klicken Sie oben rechts im Browser auf <i class="fa fa-bars" aria-hidden="true"></i><span class="sr-only">die drei horizontalen Striche</span> und öffnen Sie die "Einstellungen".',
        'default-page-v52.2' => 'Tragen Sie im Feld "Startseite" ":link" ein.',

        //Instructions for Firefox version >= 61
        'default-page-v61.1' => 'Klicken Sie oben rechts im Browser auf <i class="fa fa-bars" aria-hidden="true"></i><span class="sr-only">die drei horizontalen Striche</span> und öffnen Sie die "Einstellungen".',
        'default-page-v61.2' => 'Wählen Sie auf der linken Seite die Kategorie "Startseite" ',
        'default-page-v61.3' => 'Unter "Neue Fenster und Tabs" klicken Sie auf das Dropdownmenü neben "Startseite und neue Fenster" und wählen "Benutzerdefinierte Adresse".',
        'default-page-v61.4' => 'Tragen Sie in das erscheinende Textfeld ":link" ein.',
    ];