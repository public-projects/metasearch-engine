<?php
    return[

        //Instructions for Edge version 15-17
        'default-search-v15.1' => 'Klicken Sie in Ihrem Browser oben rechts auf "<i class="fa fa-ellipsis-h"></i>" und wählen Sie "Einstellungen".',
        'default-search-v15.2' => 'Scrollen Sie nach unten und klicken auf "Erweiterte Einstellungen".',
        'default-search-v15.3' => 'Scrollen Sie erneut nach unten bis zum Punkt "In Adressleiste suchen mit" und klicken Sie auf "Ändern".',
        'default-search-v15.4' => 'Wählen Sie "MetaGer: Sicher suchen & finden..." und klicken Sie auf "Als Standard"',

        //Instructions for Edge version 18
        'default-search-v18.1' => 'Klicken Sie in Ihrem Browser oben rechts auf "<i class="fa fa-ellipsis-h"></i>" und wählen Sie "Einstellungen".',
        'default-search-v18.2' => 'Wählen Sie den Reiter "Erweitert".',
        'default-search-v18.3' => 'Scrollen Sie nach unten bis zum Punkt "In Adressleiste suchen mit" und klicken Sie auf "Ändern".',
        'default-search-v18.4' => 'Wählen Sie "MetaGer: Sicher suchen & finden..." und klicken Sie auf "Als Standard".',

        //Instructions for Edge version >= 80
        'default-search-v80.1' => 'Geben Sie "edge://settings/searchEngines" in der Adressleiste ein um in die Suchmaschineneinstellungen zu gelangen.',
        'default-search-v80.2' => 'Klicken Sie neben dem Eintrag von MetaGer auf "<i class="fa fa-ellipsis-h"></i>" und wählen Sie "Als Standard".',

        'default-page-v15.1' => 'Klicken Sie oben rechts im Browser auf "<i class="fa fa-ellipsis-h"></i>" und öffnen Sie die "Einstellungen".',
        'default-page-v15.2' => 'Wählen Sie unter "Startseite festlegen" bzw. "Edge öffnen mit" "eine bestimmte Seite" und tragen ":link"',
        'default-page-v15.3' => 'Klicken Sie auf <i class="fas fa-save"></i> um MetaGer als Standardsuchmaschine zu speichern.',

        'default-page-v80.1' => 'Geben Sie "edge://settings/onStartup" in die Adressleiste ein um in die Einstellungen "Beim Start" zu gelangen.',
        'default-page-v80.2' => 'Wählen Sie "Bestimmte Seite oder Seiten öffnen" und tragen ":link" als URL bei "Neue Seite hinzufügen" ein.',
        'default-page-v80.3' => 'Hinweis: Alle hier sichtbaren Webseiten werden nun beim Start des Browsers geöffnet. Sie können Einträge entfernen, indem Sie mit der Maus drüber fahren und rechts auf "<i class="fa fa-ellipsis-h"></i>" klicken.',

    ];