<?php
    return[
        
        //Instructions for Chrome version 49-52
        'default-search-v49.1' => 'Klicken Sie in Ihrem Chrome oben rechts auf <i class="fas fa-bars"></i> und im folgenden Menü auf "Einstellungen", um die Einstellungen Ihres Chromes zu öffnen.',
        'default-search-v49.2' => 'Im Bereich "Suche" klicken Sie auf "Suchmaschinen verwalten...".',
        'default-search-v49.3' => 'In der unteren Hälfte des erscheinenden Menüs fahren Sie mit der den MetaGer-Eintrag. Auf der rechten Seite des Eintrags klicken sie auf die erscheinende blaue "Als Standard"-Schaltfläche',

        //Instructions for Chrome version 53-58
        'default-search-v53.1' => 'Klicken Sie in Ihrem Chrome oben rechts auf <i class="fa fa-ellipsis-v"></i> und im folgenden Menü auf "Einstellungen", um die Einstellungen Ihres Chromes zu öffnen.',
        'default-search-v53.2' => 'Im Bereich "Suche" klicken Sie auf "Suchmaschinen verwalten...".',
        'default-search-v53.3' => 'In der unteren Hälfte des erscheinenden Menüs fahren Sie mit der den MetaGer-Eintrag. Auf der rechten Seite des Eintrags klicken sie auf die erscheinende blaue "Als Standard"-Schaltfläche',

        //Instructions for Chrome version >=59
        'default-search-v59.1' => 'Klicken Sie in Ihrem Chrome oben rechts auf <i class="fa fa-ellipsis-v"></i> und im folgenden Menü auf "Einstellungen", um die Einstellungen Ihres Chromes zu öffnen.',
        'default-search-v59.2' => 'Im Bereich "Suchmaschine" klicken Sie auf "Suchmaschinen verwalten..."',
        'default-search-v59.3' => 'In der nun angezeigten Liste, finden Sie den Eintrag "MetaGer". Fahren Sie mit der Maus über den Eintrag und klicken rechts auf <i class="fa fa-ellipsis-v"></i> und anschließend auf "Als Standard festlegen"',

        'default-page-v49.1' => 'Klicken Sie oben rechts im Browser auf <i class="fa fa-ellipsis-v"></i> und öffnen Sie die "Einstellungen".',
        'default-page-v49.2' => 'Im Bereich "Beim Start" wählen Sie "Bestimmte Seite oder Seiten öffnen und klicken anschließend auf "Neue Seite hinzufügen" bzw. "Seite festlegen".',
        'default-page-v49.3' => 'Tragen Sie ":link" als URL bei "Neue Seite hinzufügen" ein.',
        'default-page-v49.4' => 'Hinweis: Alle hier sichtbaren Webseiten werden nun beim Start des Browsers geöffnet. Sie können Einträge entfernen, indem Sie mit der Maus drüber fahren und rechts auf das "x" klicken.',
    ];