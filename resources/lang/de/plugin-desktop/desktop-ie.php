<?php
    return[

        //Instructions for IE 9-10
        'default-search-v9.1' => 'Klicken Sie <a href="javascript:window.external.addSearchProvider($(\'link[rel=search]\').attr(\'href\'));">hier</a>, um MetaGer als Suchmaschine hinzuzufügen.',
        'default-search-v9.2' => 'Wählen Sie "Diese Suchmaschine als Standard einstellen" aus und klicken auf "Hinzufügen".',

        //Instructions for IE 11
        'default-search-v11.1' => 'Klicken Sie <a href="javascript:window.external.addSearchProvider($(\'link[rel=search]\').attr(\'href\'));">hier</a>, um MetaGer als Suchmaschine hinzuzufügen.',
        'default-search-v11.2' => 'Klicken Sie in Ihrem Browser oben rechts auf "<i class="fa fa-cog" aria-hidden="true"></i>".',
        'default-search-v11.3' => 'Wählen Sie den Menüpunkt "Add-Ons verwalten".',
        'default-search-v11.4' => 'Klicken Sie im Bereich "Add-On-Typen" auf "Suchanbieter" und anschließend im rechten Bereich auf MetaGer.',
        'default-search-v11.5' => 'Bestätigen Sie unten mit dem Knopf "Als Standard".',

        'default-page-v9.1' => 'Klicken Sie <a href="/" target="_blank" rel="noopener">hier</a> um MetaGer in einem neuen Tab zu öffnen.',
        'default-page-v9.2' => 'Im neuen Tab klicken Sie oben links auf den Pfeil neben dem <i class="fas fa-home" aria-hidden="true"></i> und anschließend auf "Startseite ändern oder hinzufügen".',
        'default-page-v9.3' => 'Im erscheinenden Popup wählen Sie "Diese Seite als einzige Startseite nutzen" und anschließend auf "Ja".',
    ];