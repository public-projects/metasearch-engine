<?php
    return[

        //Instructions for Opera versions >=36
        'default-search-v36.1' => 'Klicken Sie mit der rechten Maustaste auf das Sucheingabefeld am Ende dieser Anleitung.',
        'default-search-v36.2' => 'Wählen Sie im Kontextmenü "Suchmaschine erstellen...".',
        'default-search-v36.3' => 'Klicken Sie im Popup auf "erstellen".',
        //link to desktop-unable

        'default-page-v36.1' => 'Klicken Sie oben links im Browser auf das Opera-Symbol bzw. "Menü" und öffnen Sie die "Einstellungen".',
        'default-page-v36.2' => 'Im Bereich "Beim Start" wählen Sie "Bestimmte Seite oder Seiten öffnen und klicken anschließend auf "Neue Seite hinzufügen" bzw. "Seite festlegen".',
        'default-page-v36.3' => 'Tragen Sie ":link" als URL bei "Neue Seite hinzufügen" ein.',
        'default-page-v36.4' => 'Hinweis: Alle hier sichtbaren Webseiten werden nun beim Start des Browsers geöffnet. Sie können Einträge entfernen, indem Sie neben dem Eintrag auf "<i class="fas fa-ellipsis-h"></i>" klicken.',
    ];