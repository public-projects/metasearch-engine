<?php
    return[

        //Instructions for Vivaldi version 3.3
        'default-search-v3-3.1' => 'Klicken Sie mit der rechten Maustaste auf das Sucheingabefeld unter der Anleitung.',
        'default-search-v3-3.2' => 'Wählen Sie im Kontextmenü "Suche erstellen..."',
        'default-search-v3-3.3' => 'Klicken Sie im Popup auf "Als Standard-Suche festlegen" und anschließend auf "Hinzufügen".',

        'default-page-v3-3.1' => 'Klicken Sie oben links auf das Vivaldi-Symbol und wählen Sie unter "Extras" den Menüpunkt "<i class="fa fa-cog" aria-hidden="true"></i> Einstellungen"',
        'default-page-v3-3.2' => 'Im erscheinenden Fenster unter "Startseite" wählen Sie  "Benutzerdefiniert:" und tragen :link ein.',
    ];