<?php

return [
    'open-modal' => 'Install MetaGer-Plugin',
    'open-modal.title' => 'Add the MetaGer-Plugin to your browser',
    "head.1" => "Add MetaGer to your Firefox",
    "head.2" => "Add MetaGer to your Chrome",
    "head.3" => "Add MetaGer to your Opera",
    "head.4" => "Add MetaGer to your Internet Explorer",
    "head.5" => "Add MetaGer to your Microsoft Edge",
    "head.6" => "Add MetaGer to your Safari",
    "head.7" => "Add Metager to your Vivaldi",
    "head.8" => "Add MetaGer to your Firefox Focus",

    'browser-download' => 'Download Firefox',
    'firefox-plugin' => 'Set MetaGer as default search engine with add-on',
    'firefox-default-search' => 'Set MetaGer as default search engine without add-on',
    'default-search' => 'Set MetaGer as default search engine',
    'default-page' => 'Set MetaGer as start page',

    'desktop-unable' => 'The browser you use does not offer to add MetaGer as (default) search engine, but you can download Firefox <a href="https://www.mozilla.org/en-US/firefox/new/" target="_blank" rel="noopener">here</a>, an open-source browser which provides the feature.',
    'mobile-unable' => 'The browser you use does not offer to add MetaGer as (default) search engine, but you can download Firefox <a href="https://www.mozilla.org/en-US/firefox/mobile/" target="_blank" rel="noopener">here</a>, an open-source browser which provides the feature.',

    'desktop-unlisted' => 'We are not aware if the browser you use offers to add MetaGer as (default) search engine, but you can download Firefox <a href="https://www.mozilla.org/en-US/firefox/new/" target="_blank" rel="noopener">here</a>, an open-source browser which provides the feature.',
    'mobile-unlisted' => 'We are not aware if the browser you use offers to add MetaGer as (default) search engine, but you can download Firefox <a href="https://www.mozilla.org/en-US/firefox/mobile/" target="_blank" rel="noopener">here</a>, an open-source browser which provides the feature.',

    ];
