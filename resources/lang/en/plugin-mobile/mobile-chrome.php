<?php
    return[

        'default-search-v83.1' => 'Tap "<i class="fas fa-ellipsis-v"></i>" in the top right corner.',
        'default-search-v83.2' => 'Choose "Settings".',
        'default-search-v83.3' => 'Under "Basics" pick "Search engine".',
        'default-search-v83.4' => 'Under "Recently Visited" choose MetaGer.',
        'default-search-v83.5' => 'If MetaGer does not appear in the list use the search bar at the end of this instructions and try again.',

        'default-page-v83.1' => 'Tap "<i class="fas fa-ellipsis-v"></i>" in the top right corner.',
        'default-page-v83.2' => 'Choose "Settings".',
        'default-page-v83.3' => 'Under "Advanced pick "Homepage".',
        'default-page-v83.4' => 'Choose "Enter custom web address" and enter ":link" .',

    ];