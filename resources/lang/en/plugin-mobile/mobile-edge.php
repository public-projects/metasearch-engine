<?php
    return[

        'default-search-v45.1' => 'Tap "<i class="fas fa-ellipsis-h"></i>" at the bottom centre to open the menu.',
        'default-search-v45.2' => 'Choose "Settings" on the right side.',
        'default-search-v45.3' => 'Scroll down und pick "Search" in the "Advanced" category.',
        'default-search-v45.4' => 'Choose "Default search engine" and pick MetaGer from the "Recently Visited" list.',
        'default-search-v45.5' => 'If MetaGer does not appear in the list use the search bar at the end of this instructions and try again.',

        'default-page-v45.1' => 'Click <a href="/" target="_blank" rel="noopener">here</a> to open MetaGer in a new Tab.',
        'default-page-v45.2' => 'Tap "<i class="fas fa-ellipsis-h"></i>" at the bottom centre to open the menu and shoose "Settings" on the right side.',
        'default-page-v45.3' => 'Choose "Home page" in the "Basic" category.',
        'default-page-v45.4' => 'Choose "A specific page", tap "Use current page" and "save".',
    ];