<?php
    return[

        'search-string' => 'Search string:',

        'default-search-v8-8' => 'Tap <i class="fa fa-cog" aria-hidden="true"></i>(iOs) or <i class="fas fa-ellipsis-v"></i>(Android) in the top right corner and choose "Settings".',
        'default-search-v8-8' => 'Under "Search" choose "Search engine".',
        'default-search-v8-8' => 'Tap "Add another search engine" Enter "MetaGer" as "search engine name" and "https://metager.org/meta/meta.ger3?eingabe=%s" as "Search string to use".',
        'default-search-v8-8' => 'Be careful not to add a space at the end of the search string accidently. Otherwise Firefox Focus will not allow you to save.',
    
    ];