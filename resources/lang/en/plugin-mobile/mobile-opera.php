<?php
    return[

        'default-search-v60.1' => 'Tap and hold the search bar at the end of the instruction',
        'default-search-v60.2' => 'Choose "Add search engine...".',
        'default-search-v60.3' => 'Tap "OK" in the popup.',
        'default-search-v60.4' => 'Now you can choose MetaGer as search engine by tapping the icon in the top left corner of a new tab.',        
        //link to mobile-unable
    ];