<?php
    return[

        'search-string' => 'Search string:',

        //Instructions for Firefox Mobile version < 80
        'default-search-vlt80.1' => 'Tap and hold the search bar at the end of the instructions.',
        'default-search-vlt80.2' => 'Choose "Add search engine" from the context menu',
        'default-search-vlt80.3' => 'Now you can go to your browser settings and choose MetaGer as your default search engine in the section "Search".',

        //Instructions for Firefox Mobile version >= 80
        'default-search-v80.1' => 'Tap "<i class="fas fa-ellipsis-v"></i>" in the bottom right corner.',
        'default-search-v80.2' => 'Choose Settings.',
        'default-search-v80.3' => 'Under "General" pick "Search".',
        'default-search-v80.4' => 'Tap "+ Add search engine".',
        'default-search-v80.5' => 'Choose "Other", enter MetaGer as name and the search string to use. Finally tap "<i class="fas fa-check"></i>" in the top right corner to save.',
    ];