<?php

return [
    "foki.web" => "Web",
    "foki.bilder" => "Pictures",
    "foki.nachrichten" => "News/Politics",
    "foki.wissenschaft" => "Science",
    "foki.produkte" => "Shopping",
    "foki.angepasst" => "Customized",
    "foki.maps" => "maps.metager.de",

    "design" => "Select personal theme",

    "conveyor" => "Purchase at affiliate shops",
    "partnertitle" => "Support MetaGer without any costs to you",
    "mapstitle" => "MetaGer Maps Service",

    "plugin" => "Add MetaGer-Plugin",
    "plugin-title" => "Add MetaGer to your browser",

    'options.head' => 'Customize filters',

    "focus-creator.head" => "Customize search",
    "focus-creator.description" => "Arrange a personal search",
    "focus-creator.name-placeholder" => "Label of search focus",
    "focus-creator.save" => "Save search focus",
    "focus-creator.delete" => "Delete search focus",
    "focus-creator.focusname" => "Focus:",

    'about.1.1' => "Guaranteed Privacy",
    'about.1.2' => 'With us you have full control over your data. We don\'t track and our source code is free.',
    'about.2.1' => 'Diverse & free',
    'about.2.2' => 'MetaGer protects against censorship by combining the results of multiple search engines.',
    'about.3.1' => '100 % renewable energy',
    'about.3.2' => 'All of our services are run using renewable energy. Sustainable and clean.',
    'about.4.1' => 'Nonprofit organization',
    'about.4.2' => 'Strengthen us by becoming a member or by donating to our nonprofit organization!',

    'lang.tooltip' => 'Language',
    'key.placeholder' => 'Enter member key',
    'key.tooltip' => 'Enter member key',

    "placeholder" => "MetaGer: Privacy Protected Search & Find",
    'searchbutton' => 'MetaGer-Search',

    'tooltips.add-focus' => 'Adjust search',
    'tooltips.edit-focus' => 'Change the actual focus',
    'tooltips.settings' => 'Common settings',
];
