<?php
return [
    '1' => 'Sorry to bother you',
    '2' => 'You are in a network from which we increasingly receive automated requests. Do not worry: this does not necessarily mean that these requests come from your PC.',
    '3' => 'However, we can not distinguish your requests from those of the "robot". To protect the search engines we query, we must ensure that they are not flooded with (automated) queries.',
    '4' => 'Please enter the characters from the picture in the input box and confirm with "OK" to get to the result page.',
    '5' => 'Enter captcha',
    '6' => 'Enter weekday',
];
