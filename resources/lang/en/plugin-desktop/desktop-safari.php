<?php
    return[

        //link to unable
        'default-page-v10.1' => 'Click <a href="/" target="_blank" rel="noopener">here</a> to open MetaGer in a new tab.',
        'default-page-v10.2' => 'In the top left corner click on "Safari" and choose "Preferences".',
        'default-page-v10.3' => 'Under "General" click the "Set to Current Page" button.'
    ];