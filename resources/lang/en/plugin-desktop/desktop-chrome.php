<?php
    return[
        
        //Instructions for Chrome version 49-52
        'default-search-v49.1' => 'Navigate to the top right corner of your browser, click "<i class="fas fa-bars"></i>" and choose "Settings".',
        'default-search-v49.2' => 'Under "Search" click the button "Manage search engines...".',
        'default-search-v49.3' => 'Move your mouse over the "MetaGer" entry and click on "Make Default".',
        //Instructions for Chrome version 53-58
        'default-search-v53.1' => 'Navigate to the top right corner of your browser, click "<i class="fa fa-ellipsis-v"></i>" and choose "Settings".',
        'default-search-v53.2' => 'Under "Search" click the button "Manage search engines...".',
        'default-search-v53.3' => 'Move your mouse over the "MetaGer" entry and click on "Make Default".',
        
        //Instructions for Chrome version >=59
        'default-search-v59.1' => 'Navigate to the top right corner of your browser, click "<i class="fa fa-ellipsis-v"></i>" and choose "Settings".',
        'default-search-v59.2' => 'Under "Search engine" click the button "Manage search engines".',
        'default-search-v59.3' => 'Click on "<i class="fa fa-ellipsis-v"></i>" next to the MetaGer entry and choose "Make default".',

        'default-page-v49.1' => 'Navigate to the top right corner of your browser, click "<i class="fa fa-ellipsis-v"></i>" and choose "Settings".',
        'default-page-v49.2' => 'Under "On startup" choose "Open a specific page or set of pages." and click "Set pages".',
        'default-page-v49.3' => 'Enter ":link" as URL and click OK.',
        'default-page-v49.4' => 'Hint: Every listed page in this window will be opened on startup. You can remove entries by moving your mouse over them and click "x".',
    ];