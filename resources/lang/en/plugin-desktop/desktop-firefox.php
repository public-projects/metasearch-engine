<?php
    return[

        //Instructions for Firefox version 52-56
        'default-search-v52.1' => 'Navigate to the top right corner and click next to the address bar on the magnifier icon.',
        'default-search-v52.2' => 'In the new menu click "Add "MetaGer ...""',
        'default-search-v52.3' => 'Enter "about:preferences#search" in your address bar to access the search settings.',
        'default-search-v52.4' => 'Choose MetaGer from the dropdown menu under "Default Search Engine".',

        //Instructions for Firefox version 57-60
        'default-search-v57.1' => 'Open a new tab and enter "about:preferences#search" to access the "Search" settings.',
        'default-search-v57.2' => 'Under "Search Bar" choose "Add search bar in toolbar".',
        'default-search-v57.3' => 'Go back to the MetaGer tab and click on the magnifier on the right next to the address bar.',
        'default-search-v57.4' => 'In the new menu click "Add "MetaGer ...""',
        'default-search-v57.5' => 'Go back to the Settings tab. Choose MetaGer from the dropdown menu under "Default Search Engine".',

        //Instructions for Firefox version >= 61
        'plugin' => 'You can download our plugin <a href="https://addons.mozilla.org/en-US/firefox/addon/metager-searchengine/" target="_blank">here</a>.',
        'default-search-v61.1' => 'Alternatively click "<i class="fas fa-ellipsis-h"></i>" on the right side in your address bar and choose "Add Search Engine".',
        'default-search-v61.2' => 'Enter "about:preferences#search" in the address bar. Choose MetaGer from the dropdown menu under "Default Search Engine".',

        //Instructions for Firefox version 52-60
        'default-page-v52.1' => 'Navigate to the top right corner, click "<i class="fa fa-bars"></i>" and choose "Options".',
        'default-page-v52.2' => 'Under "Home page" enter ":link" .',

        //Instructions for Firefox version >= 61
        'default-page-v61.1' => 'Navigate to the top right corner, click "<i class="fa fa-bars"></i>" and choose "Options".',
        'default-page-v61.2' => 'Chosse Home on the left side.',
        'default-page-v61.3' => 'Under "New Windows and Tabs" click on the dropdown menu next to "Homepage and new windows" and choose "Custom URLs...".',
        'default-page-v61.4' => 'In the new text field enter ":link" .',
    ];