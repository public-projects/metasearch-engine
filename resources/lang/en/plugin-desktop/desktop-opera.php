<?php
    return[

        //Instructions for Opera versions >=36
        'default-search-v36.1' => 'Right click the search bar at the end of this instruction.',
        'default-search-v36.2' => 'Choose "Create search engine..." in the context menu.',
        'default-search-v36.3' => 'Click "Create" in the popup.',
        //link to desktop-unable

        'default-page-v36.1' => 'Navigate to the top left corner and click "Opera" logo or "Menu" and click "Settings".',
        'default-page-v36.2' => 'Under "On startup" choose "Open a specific page or set of pages" and click "Add a new page".',
        'default-page-v36.3' => 'Enter ":link" as URL and click "Add".',
        'default-page-v36.4' => 'Hint: Every listed page in this window will be opened on startup. You can remove entries by moving your mouse over them and click "<i class="fas fa-ellipsis-h"></i>".',

    ];