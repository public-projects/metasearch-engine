<?php
    return[

        //Instructions for Vivaldi version 3.3
        'default-search-v3-3.1' => 'Right click the search bar at the end of this instruction',
        'default-search-v3-3.2' => 'Choose "Add as Search Engine..." in the context menu.',
        'default-search-v3-3.3' => 'Check the "Set as Default Search" box and click "Add" in the popup.',

        'default-page-v3-3.1' => 'Navigate to the top left corner, click the "Vivaldi" logo and choose "<i class="fa fa-cog" aria-hidden="true"></i> Einstellungen" under "Tools"',
        'default-page-v3-3.2' => 'Under "Homepage" choose  "Specific Page" and enter :link .',
    ];