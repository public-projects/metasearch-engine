<?php
    return[

        //Instructions for Edge version 15-17
        'default-search-v15.1' => 'Navigate to the top right corner of your browser, click "<i class="fa fa-ellipsis-h"></i>" and choose "Settings".',
        'default-search-v15.2' => 'Scroll down and click "Advanced settings".',
        'default-search-v15.3' => 'Scroll down again and under "Search in the address bar with" click "Change search engine".',
        'default-search-v15.4' => 'Choose "MetaGer: privacy protected search & find" and click "Set as default".',

        //Instructions for Edge version 18
        'default-search-v18.1' => 'Navigate to the top right corner of your browser, click "<i class="fa fa-ellipsis-h"></i>" and choose "Settings".',
        'default-search-v18.2' => 'Click on "advanced" on the left side of the new menu',
        'default-search-v18.3' => 'Scroll down and under "Address bar search" click "Change search provider".',
        'default-search-v18.4' => 'Choose "MetaGer: privacy protected search & find" and click "Set as default".',

        //Instructions for Edge version >= 80
        'default-search-v80.1' => 'Open a new tab and enter "edge://settings/searchEngines" in your address bar to access the search engine settings.',
        'default-search-v80.2' => 'Click on "<i class="fa fa-ellipsis-h"></i>" next to the MetaGer entry and choose "Make default".',

        'default-page-v15.1' => 'Navigate to the top right corner of your browser, click "<i class="fa fa-ellipsis-h"></i>" and choose "Settings".',
        'default-page-v15.2' => 'Choose under "Open Microsoft Edge with" "A specific page or pages" and enter ":link".',
        'default-page-v15.3' => 'By clicking "<i class="fas fa-save"></i>" you add MetaGer to the start up list.',

        'default-page-v80.1' => 'Open a new tab and enter "edge://settings/onStartup" in your address bar to access the start-up settings',
        'default-page-v80.2' => 'Choose "Open a specific page or pages" and enter ":link" as URL in "Add a new page" ein. Click "Add".',
        'default-page-v80.3' => 'Hint: Every listed page in this window will be opened on startup. You can remove entries by moving your mouse over them, click "<i class="fa fa-ellipsis-h"></i>" and choose "Delete".',

    ];