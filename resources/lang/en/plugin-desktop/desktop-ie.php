<?php
    return[

        //Instructions for IE 9-10
        'default-search-v9.1' => 'Click <a href="javascript:window.external.addSearchProvider($(\'link[rel=search]\').attr(\'href\'));">here</a> to add MetaGer as search engine.',
        'default-search-v9.2' => 'Check the "Make this my default search provider" box and click "Add".',

        //Instructions for IE 11
        'default-search-v11.1' => 'Click <a href="javascript:window.external.addSearchProvider($(\'link[rel=search]\').attr(\'href\'));">here</a>, to add MetaGer as search engine.',
        'default-search-v11.2' => 'Navigate to the top right corner and click "<i class="fa fa-cog" aria-hidden="true"></i>".',
        'default-search-v11.3' => 'Choose "Manage Add-Ons".',
        'default-search-v11.4' => 'On the left choose "Search Providers" and click MetaGer on the right.',
        'default-search-v11.5' => 'Click "Set as default" on the bottom right corner of the window',

        'default-page-v9.1' => 'Click <a href="/" target="_blank" rel="noopener">here</a> to open MetaGer in a new tab.',
        'default-page-v9.2' => 'In the new tab click the arrow next to the <i class="fas fa-home" aria-hidden="true"></i> on the top left and choose "Add or change home page".',
        'default-page-v9.3' => 'In the popup choose "Use this webpage as your only home page" and click "Yes".',
    ];